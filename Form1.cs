﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace mixing_words
{
    public partial class Form1 : Form
    {
        public bool _MixByLine = false;
        public bool _MixBlank = false;

        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            
            linkLabel1.Click += new EventHandler(linkLabel1_Click);
            button1.Click += new EventHandler(button1_Click);
        }

        void linkLabel1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://programs.esukmean.com/");
        }
        string mixing(string str, string spilter, string separator)
        {
            str = str.Trim();
            string[] items = null;
            if (spilter != "")
            {
                items = str.Split(spilter.ToCharArray());
            }
            else
            {
                char[] str_tmp = str.ToCharArray();
                items = new string[str_tmp.Length];
                for (int i = 0; i < str_tmp.Length; i++)
                {
                    items[i] = str_tmp[i].ToString();
                }
            }

            if (items.Length < 2) return null;
            
            //_MixSpace가 false면 스페이스 제거
            if (! _MixBlank)
            {
                string[] items_tmp = new string[items.Length];

                int j = 0;
                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] != "") items_tmp[j++] = items[i];

                }

                items = new string[j];
                Array.Copy(items_tmp, items, j);
            }

            //모두 같은 단어인지 체크
            int count = 1;
            for (; count < items.Length; ++count)
            {
                if (items[count - 1] != items[count]) break;
            }
            if (count == items.Length) return null;

            //글 섞기 코어
            //포인터를 쓸 수 있었으면 swap할때 조금이라도 메모리를 아끼고 성능향상까지 할 수 있었을텐데...
            Random r = new Random();
            string swap = "";
            int rand = 0;
            for (count = 0; count < items.Length; ++count)
            {
                rand = r.Next(0, items.Length);

                swap = items[count];
                items[count] = items[rand];
                items[rand] = swap;
            }

            //맨 마지막으로 정렬.
            //string을 일일이 붙이면 계속 오브젝트가 생성되기에... 성능을 위해 StringBuilder 사용.
            //원래 길이 + (구분자 길이 * 구분자가 들어갈 횟수)
            System.Text.StringBuilder SB = new System.Text.StringBuilder(str.Length + ((items.Length - 1) * separator.Length) + 10);
            int last = items.Length - 1;
            for (count = 0; count < items.Length; ++count)
            {
                SB.Append(items[count]);

                if (count == last) break;
                    SB.Append(separator);
            }

            return SB.ToString();
        }

        void button1_Click(object sender, EventArgs e)
        {
            string mixed = mixing(textBox3.Text, textBox1.Text, textBox2.Text);
            if (mixed == null)
            {
                MessageBox.Show("섞을 수 없는 글 입니다.", "글 섞기 오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            richTextBox2.Text = mixed;
        }
    }
}
