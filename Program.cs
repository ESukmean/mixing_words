﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace mixing_words
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            try
            {
                string ver = "";
                if ((ver = new System.Net.WebClient().DownloadString("https://update.esukmean.com/check/mixing_words")) != "2.0.1")
                {
                    /*
                     * if (MessageBox.Show("Mixing Words의 업데이트가 필요합니다. 업데이트 하시겠습니까?\r\n(“예”를 누르면 다운로드 페이지가 열립니다.) \r\n\r\n현재 버전: 2.0.1\r\n최신 버전: " + ver, "Mixing Words - 글 섞기 업데이트 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start("https://update.esukmean.com/update/mixing_words/2.0.1");
                        System.Environment.Exit(0);
                    }
                     * */
                }

            }
            catch
            {

            }

            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
